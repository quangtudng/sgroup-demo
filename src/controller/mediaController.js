const { response } = require("express");
const { uploadCloudinary } = require("../helpers/cloudinaryUpload");
const { HTTPException } = require("../helpers/errorHandler");

const uploadSingle = async (req, res, next) => {
  try {

    let data = await uploadCloudinary(req.file.path);
    data = null;
    if(!data) {
      throw new HTTPException(400, "Some shit happened");
    }
    return res.json({
      status: "success",
      statusCode: 200,
      data,
    });
  } catch (error) {
    return next(error);
  }
};

const uploadMany = async (req, res, next) => {
  try {
    const promises = [];
    for (let i = 0; i < req.files.length; i++) {
      promises.push(uploadCloudinary(req.files[i].path));
    }
    const data = await Promise.all(promises);
    return res.json({
      status: "success",
      statusCode: 200,
      data,
    });
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  uploadSingle,
  uploadMany,
}