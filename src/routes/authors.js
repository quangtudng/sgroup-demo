var express = require('express');
var router = express.Router();
var indexController = require('../controller/authorController');
var m = require('../middleware/authors');
var { checkAuthentication } = require('../middleware/authentication');
const { checkAuthorization } = require('../middleware/authorization');

router.post('/authors', m.validatePost, checkAuthentication, checkAuthorization("CREATE_AUTHOR"), indexController.createOne);
router.get('/authors/:id', m.validateGetOne, checkAuthentication, checkAuthorization("READ_AUTHOR"), indexController.getOne);
router.get('/authors', m.validateGetAll, checkAuthentication, checkAuthorization("READ_AUTHOR"), indexController.getAll);
router.patch('/authors/:id', m.validatePatchOne, checkAuthentication, checkAuthorization("UPDATE_AUTHOR"), indexController.patchOne);
router.delete('/authors/:id', m.validateDeleteOne, checkAuthentication, checkAuthorization("DELETE_AUTHOR"), indexController.deleteOne);

module.exports = router;
