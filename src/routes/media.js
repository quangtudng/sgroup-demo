const express = require('express');

const router = express.Router();

const controller = require('../controller/mediaController');
const { uploadMulter } = require('../helpers/imageUpload');

router.post('/upload-single', uploadMulter('image', false), controller.uploadSingle);

router.post('/upload-many', uploadMulter('images', true, 5), controller.uploadMany);

module.exports = router;