const knex = require('../config/knex/connection');

const getOneById = (id) => {
  return knex('authors').where({ id }).first();
}
module.exports = {
  getOneById,
}