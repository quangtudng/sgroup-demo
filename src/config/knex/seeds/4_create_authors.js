
const faker = require('faker');
const bcrypt = require('bcrypt');
exports.seed = async function(knex) {
  const authors = [];
  const password = await bcrypt.hash("123456", 10);
  authors.push({ email: "admin@gmail.com", password, role_id: 1 });
  authors.push({ email: "user@gmail.com", password, role_id: 2 });
  for (let i = 0; i < 98; i++) {
    const email = faker.internet.email();
    authors.push({ email, password, role_id: 2 });
  }
  return knex('authors').del()
    .then(function () {
      return knex('authors').insert(authors);
    });
};
